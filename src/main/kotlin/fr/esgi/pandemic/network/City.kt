package fr.ca.cats.fr.esgi.pandemic.network

private const val MAX_INFECTION_LEVEL = 3

data class City(val name: String, val infectionMarks: Int = 0) {

    fun infect(): City {
        return if (infectionMarks < MAX_INFECTION_LEVEL) copy(infectionMarks = infectionMarks + 1) else this
    }
}
