package fr.esgi.pandemic.infection

import fr.ca.cats.fr.esgi.pandemic.network.City
import io.cucumber.java.ParameterType
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.assertj.core.api.Assertions

class InfectionSteps {
    private val currentCities: MutableMap<String, City> = mutableMapOf()

    @ParameterType("PARIS|LONDON")
    fun cityName(cityName: String): String {
        return cityName
    }

    @Given("{cityName} has not been infected")
    fun cityHasNotBeenInfected(cityName: String) {
        currentCities[cityName] = City(cityName, 0)
    }

    @Given("{cityName} has been infected {int} time(s)")
    fun cityHasNotBeenInfectedTime(cityName: String, times:Int) {
        currentCities.getOrPut(cityName) { City(cityName, 0) }
        for (i in times downTo 1) {
            currentCities[cityName] = currentCities[cityName]!!.infect()
        }
    }

    @When("{cityName} is infected")
    fun parisIsInfected(cityName: String) {
        currentCities[cityName] = currentCities[cityName]?.infect()?:throw IllegalStateException("city $cityName does not exist on network")
    }

    @Then("{cityName} infection marks should be {int}")
    fun parisInfectionMarksShouldBe(cityName: String, markers: Int) {
        Assertions.assertThat(currentCities[cityName]!!.infectionMarks).isEqualTo(markers)
    }
}