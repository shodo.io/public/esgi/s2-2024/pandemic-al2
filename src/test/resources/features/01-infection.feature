Feature: City Infection

  Rule: City infection should not increment markers

    Scenario: First Infection
      Given LONDON has not been infected
      And PARIS has not been infected
      When LONDON is infected
      Then LONDON infection marks should be 1

    Scenario: Second Infection
      Given LONDON has been infected 1 time
      When LONDON is infected
      Then LONDON infection marks should be 2

    Scenario: Second Infection
      Given LONDON has been infected 2 times
      When LONDON is infected
      Then LONDON infection marks should be 3

  Rule: Fourth Infection should not increment markers

    Scenario: Fourth Infection
      Given LONDON has been infected 3 times
      When LONDON is infected
      Then LONDON infection marks should be 3
